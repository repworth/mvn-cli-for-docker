FROM delitescere/jdk:1.8.0_60

RUN \
		wget http://www.eu.apache.org/dist/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz && \
		tar -xzvf ./*gz && \
		rm ./*gz && \
		mv apache-maven-3.3.9 /usr/lib/mvn/ && \
		ln -s /usr/lib/mvn/bin/mvn /usr/bin/mvn